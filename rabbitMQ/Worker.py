import pika

print("Collegamento a RabbitMQ...\n")
#Parametri di connessione. Ci colleghiamo alla macchina locale.
params = pika.ConnectionParameters(host="localhost")
#Connessione che blocca il producer in aggancio con la excange.
connection = pika.BlockingConnection(params)
#Otteniamo un canale
channel = connection.channel()
#Creo una coda
channel.queue_declare(queue="worker_queue")
print("...eseguito!")

#callback=funzione invocata automaticamente quando viene invocata la coda.

def myCallback(ch, method, properties, body):
    print("Ricevuto %s",     body)

channel.basic_consume("worker_queue", myCallback, auto_ack=True)
channel.start_consuming()