#E' un message handler per applicazioni distribuite
#Ad esempio IOT tanti utenti che accedono a medesime risorse.

#Modello PRODUCER-CONSUMER

#Producer=Notifica il messaggio. Appllicazioni che producono informazioni.
#Consumer=Aspetta di ricevere il messaggio

#Il mesage brocker si trova tra producer e consumer (disaccoppia producer e consumer).

#Produce 100000 messaggi che poi verranno consegnati tramite un unica coda a 3 worker.

import pika

print("Collegamento a RabbitMQ...\n")
#Parametri di connessione. Ci colleghiamo alla macchina locale.
params = pika.ConnectionParameters(host="localhost")
#Connessione che blocca il producer in aggancio con la excange.
connection = pika.BlockingConnection(params)
#Otteniamo un canale
channel = connection.channel()
#Creo una coda
channel.queue_declare(queue="worker_queue")
print("...eseguito!")

#creo 100000 messaggi
i=0
while True:
    message=str(i)
    i+=1
    #Pubblicare il messaggio
    channel.basic_publish(exchange='', routing_key='worker_queue', body=message)
    print("Inviato messaggio %s\n", message)
    
    if i>100_000:
        break