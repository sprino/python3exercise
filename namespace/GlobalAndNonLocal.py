#NAMESPACE:
#Mappatura tra nomi ed oggetti
#Evita collisioni tra i nomi
#Univocità dei nomi
#Per ogni programma ci sono n namespace in forma gerarchica

#SCOPE:
#Contesto, area di codice dove c isi trova durante l'exe e determina quale namespace a usare.

#Gerarchia LEGB
# local--> Enclose --> Global --> BuiltIn

#Variable hiding
#La dichiarazione a livello gerarchico più basso sovrascrive quella a livello piu alto
print("\nGlobal:")
x=100
def myFunc(y):
    x = y
    return x 
print(myFunc(333))
print(x)

print("\nGlobal:")

#Alteriamo: GLOBAL
xx=100
def myFunc2(y):
    global xx
    xx = y
    return xx 
print(myFunc2(333))
print(xx)

print("\nNonLocal:")

#Alteriamo: NonLocal
#Ad innel diciamo che la variabile nonLocal non è locale quindi python passa a livello superiore, 
#la trova dentro outer e la usa alterando il suo valore.
def outer():
    nonLocal = 20
    def inner():
        nonlocal nonLocal
        nonLocal = 1000
        print(nonLocal)
    inner()
    print(nonLocal)
outer()