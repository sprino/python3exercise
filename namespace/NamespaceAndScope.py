#NAMESPACE:
#Mappatura tra nomi ed oggetti
#Evita collisioni tra i nomi
#Univocità dei nomi
#Per ogni programma ci sono n namespace in forma gerarchica

#SCOPE:
#Contesto, area di codice dove c isi trova durante l'exe e determina quale namespace a usare.

#Gerarchia LEGB
# local--> Enclose --> Global --> BuiltIn

#Local
def sum(x,y):
    c=x+y
    return c #x,y e c formano il namespace della funzione sum

#Enclosed Scope
#Definiti in una funzione che racchiude la nostra funzione
def outer(x): #x ed y sono definiti in outer e non in inner
    y=20
    def inner():
        print(x+y) #x ed y non sono in inner, passa al livello più alto, outer
    inner()
    
#Global
#livello programma
x=10 #x è definita fuori dalla funzione, non è interna e non ci sono funzioni annidiate
def func(y):
    print(x+y)
    
#Built in (predefinito)
print('python') #print non è mai stata definita da noi ma possiamo usarla perchè si trova nel namespace di altissimo livello.