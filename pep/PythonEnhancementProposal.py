#PEP Python Enhancement Proposal
#Documenti che presentano novità o innovazioni per il linguaggio.
#Vengono presentati alla comunità ed eventualmente approvati oppure no.

#PEP-3107---Function Annotations
print("\n\nPEP-3107---Function Annotations")
def func1(a:'primo parametro',b:'secondo parametro'=5):
    pass
def func2() -> 'annotazione ritorno':
    pass
    
print("Dizionario delle annotazioni della funzione 1 :" + str(func1.__annotations__))
print("Espressione del paramentro b dlla funzione 1 :" + func1.__annotations__['b'])
print("Dizionario delle annotazioni della funzione 2 :" + str(func2.__annotations__))

#PEP-484---Type Hints
print("\n\nPEP-484---Type Hints")
def myFunc1(x,s='Python'):
    print(x)
    return s

myF = myFunc1(10)
print(myF)

def myFunc2(x:int,s:str='Python') -> str:
    print(x)
    return s

print("\n")
print(myFunc2.__annotations__)
print(myFunc2(10))

#PEP-526---Type Hints
print("\n\nPEP-526---Syntax for variable annotations---posso indicare i tipi")
var:int=100
print(var)
print(__annotations__)

print("\nTipi nelle classi")
class MyClass:
    nome:str
    cognome:str
    
    def __init__(self,nome,cognome):
        self.nome=nome
        self.cognome=cognome
        
mc=MyClass("Mario","Rossi")
print(mc)
print(mc.nome)
print(mc.cognome)