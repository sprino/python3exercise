#Classe che contiene operazioni per il conto.

class GestoreContoCorrente:
    @staticmethod
    def bonifico(contoSorgente,contoDestinazione,importo):
        contoSorgente.preleva(int(importo))
        contoDestinazione.deposita(int(importo))