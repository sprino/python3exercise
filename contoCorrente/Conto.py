# Esercizio Conto corrente.
# Classe conto corrente con costruttore con 3 parametri (nome,conto,importo)
# nome,conto,saldo attributi di istanza
# metodo che preleva                  | preleva
# Metodo che versa                    | versa
# Metodo che vede i dati del conto    | descrizione
# Script Conto1.py per creare due conti e testare.
# Property su saldo per nasconderlo.


class Conto:

    def __init__(self, nome, conto):
        self.nome = nome
        self.conto = conto
