# Esercizio Conto corrente.
# Classe conto corrente con costruttore con 3 parametri (nome,conto,importo)
# nome,conto,saldo attributi di istanza
# metodo che preleva                  | preleva
# Metodo che versa                    | versa
# Metodo che vede i dati del conto    | descrizione
# Script Conto1.py per creare due conti e testare.

from contoCorrente.Conto import Conto

class ContoCorrente(Conto):
    
    def __init__(self, nome, conto, importo):
        super().__init__(nome, conto)
        self.__saldo = int(importo)
    
    def preleva(self, importo):
        if int(importo) < self.__saldo: 
            self.__saldo -= int(importo) 
        else: 
            print("L'importo richiesto supera il tuo saldo.")
            
    def deposita(self, deposito):
        self.__saldo += int(deposito)
    
    def descrizione(self):
        print("Nome: " + self.nome + "\n" + 
                     "Conto: " + self.conto + "\n" + 
                     "Saldo:" + str(self.__saldo) +
                     " €")
    
    @property
    def saldo(self):
        return self.__saldo
    
    @saldo.setter
    def saldo(self,importo):
        self.preleva(self.__saldo)
        self.deposita(int(importo))
