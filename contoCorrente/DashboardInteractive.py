from contoCorrente.ContoCorrente import ContoCorrente
from contoCorrente.GestoreContoCorrente import GestoreContoCorrente

def clear():
    print("\n" * 1000)

contiDict = {};

def creaConto():
    nome = input("Inserisci il nome: ")
    conto = input("Inserisci il numero: ")
    importo = input("Inserisci importo iniziale:")
    contiDict[conto] = ContoCorrente(nome, conto, importo)
    return None

 
def depositaConto():
    conto = input("Numero del conto sul quale depositare: ")
    importo = input("Importo da depositare: ")
    if conto in contiDict :
        contiDict[conto].deposita(importo)
    return None

 
def prelevaConto():
    conto = input("Nome del conto dal quale prelevare: ")
    importo = input("Importo da prelevare: ")
    if conto in contiDict :
        contiDict[conto].preleva(importo)
    return None


def effettuaBonifico():
    contoDestinatario = input("Nome conto del destinatario: ")
    contoSorgente = input("Numero del conto sorgente: ")
    importo = input("Importo del bonifico: ")
    GestoreContoCorrente.bonifico(contiDict[contoSorgente], contiDict[contoDestinatario], importo)
    return None


def visualizzaConto():
    conto = input("Numero del conto: ")
    contiDict[conto].descrizione()
    input('Premere invio per continuare!')
    return None

def menu():
    menu = ["1) Crea Conto Corrente",
            "2) Deposita sul conto corrente",
            "3) Preleva al conto",
            "4) Effettua un bonifico",
            "5) Visualizza dati conto",
            "6) Esci"]
    for i in menu:
        print(i)

exitProgram = False
while exitProgram is not True:
    clear()
    menu()
    scelta = input("PREMI IL NUMERO DESIDERATO:")
    if scelta == "1":
        creaConto()
    elif scelta == "2":
        depositaConto()
    elif scelta == "3":
        prelevaConto()
    elif scelta == "4":
        effettuaBonifico()
    elif scelta == "5":
        visualizzaConto()
    elif scelta == "6":
        exitProgram = True
        clear()
        print("Programma terminato")
    else:
        print("Scelta non consentita!")
