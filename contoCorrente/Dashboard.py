from contoCorrente.ContoCorrente import ContoCorrente
from contoCorrente.GestoreContoCorrente import GestoreContoCorrente


conto1 = ContoCorrente('Rino', "001", 10000)
conto2 = ContoCorrente('Vera', "002", 10000)

conto1.descrizione()
conto2.descrizione()

GestoreContoCorrente.bonifico(conto1, conto2, 5000)

conto1.descrizione()
conto2.descrizione()