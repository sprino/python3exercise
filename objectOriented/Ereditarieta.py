############################################################
#######################Exe 1################################
############################################################
class MyClass():
    @staticmethod
    def somma(a,b):
        return(a+b)
#Sottoclasse, si passa la classe come argomento.
class MySClass(MyClass):
    #Override del metodo della classe
    @staticmethod
    def somma(a, b):
        c = 10 + MyClass.somma(a, b)
        return c
mysclass = MySClass()
#mysclass è anche un istanza della classe?
print(isinstance(mysclass, MyClass))
print('La somma più 10 é: ' + str(mysclass.somma(25, 25)))

############################################################
#######################Exe 2################################
############################################################
class Classe:
    def setMessage(self,messg):
        self.messg = messg
    def printMessage(self):
        print(self.messg)
#Creo sotto classe
class SottoClasse(Classe):
    pass
sc1 = SottoClasse()
sc1.setMessage("Uso un istanza della sotto classe")
sc1.printMessage()