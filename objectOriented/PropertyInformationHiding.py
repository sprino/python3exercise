# Nascondere l'informazione.
# getter e setter
# nascondere gli attributi e tenere pubblici i metodi.


class MyClass():

    def __init__(self, myAtt):
        self.privAtt = myAtt
        
    def getAtt(self):
        return self.privAtt
    
    def setAtt(self, myAtt):
        self.privAtt = myAtt

    attr = property(getAtt,setAtt)
    
obj = MyClass('Init');
print(obj.attr)
obj.attr = 'Ciao'
print(obj.attr)
#privAtt è ancora visibile
print(obj.privAtt)

#Voglio nascondere privAtt, uso i due underscore __

class MyClass2():

    def __init__(self, myAtt):
        self.__privAtt = myAtt
        
    def getAtt(self):
        return self.__privAtt
    
    def setAtt(self, myAtt):
        self.__privAtt = myAtt

    attr = property(getAtt,setAtt)

obj2 = MyClass2('Init');
print(obj2.attr)
obj2.attr = 'Ciao'
print(obj2.attr)
#privAtt non ancora visibile
print(obj2.privAtt)
#Python ha solo cambiato il nome dell'attributo, non è davvero nascosto
#Ha usato la tecnica di storpiamento del nome.
print(obj2._MyClass2__privAtt)
