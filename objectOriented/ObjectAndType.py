# Funzione in BClass e CClass con lo stesso nome
print('\n\n\n***Funzione in BClass e CClass con lo stesso nome***')

class MyClass:
    pass

myObj = MyClass()

print("myObj è un istanza di MyClass?\n" + str(isinstance(myObj, MyClass)))

print("myClass è un istanza di Object?\n" + str(isinstance(MyClass, object)))

print("myObj è un istanza di Object?\n" + str(isinstance(myObj, object)))

print("myObj è un istanza di Type?\n" + str(isinstance(myObj, type)))

print("MyClass è un istanza di Type?\n" + str(isinstance(MyClass, type)))

print("Object è un istanza di Object?\n" + str(isinstance(object, object)))

print("Type è un istanza di Type?\n" + str(isinstance(type, type)))

print("Object è un istanza di Type?\n" + str(isinstance(object, type)))

print("Type è un istanza di Object?\n" + str(isinstance(type, object)))
