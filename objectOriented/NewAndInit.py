#__new__ è un costruttore
#__init__ inizializza una classe
class MyClass1:
    def __new__(cls):
        print("Istanza creata")
    def __init__(self):
        print("Istanza inizializzata")
        
a = MyClass1()

print("\n")

class MyClass2:
    def __new__(cls):
        istanza = super().__new__(cls)
        print("Istanza creata")
        return istanza
    def __init__(self):
        print("Istanza inizializzata")
        
a = MyClass2()

print("\n")

class MyClass3:
    def __new__(cls, message):
        istanza = super().__new__(cls)
        return istanza
    def __init__(self, message):
        self.message = message
        print(self.message)
        
a = MyClass3('Python')