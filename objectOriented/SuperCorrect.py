# __init__ e il costruttore.
# Se ne faccio l'override nella sottoclasse il costruttore della supercalasse non viene eseguito.
# In questo esempio si ha errore perche' sc1.printMessage non l oesegue perche non trova l'attributo messg.
# La funzione super() senza argomenti si può usare da python3.
# Si può usare per tutti i metodi dei quali si fa override.


# Creo la classe
class Classe:

    def __init__(self, messg):
        self.messg = messg

    def printMessage(self):
        print(self.messg)


# Creo sotto classe
class SottoClasse(Classe):

    def __init__(self, messg, valore):
        super().__init__(messg)
        self.valore = valore

        
sc1 = SottoClasse('Message', 10)
print(sc1.valore)
sc1.printMessage()
