#__init__ costruttore della classe.

#Attributi di istanza.
#Init è un metodo di istanza.
class MyAttributeInstance:
    def __init__(self, message):
        self.message = message
    def getMessage(self):
        return self.message
        
m1 = MyAttributeInstance("Prova messaggio!")
print(m1.getMessage())

m2 = MyAttributeInstance("Secondo!")
m3 = MyAttributeInstance("Terzo!")

print(m2.getMessage())
print(m3.getMessage())
