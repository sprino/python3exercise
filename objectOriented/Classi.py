# Gli argomenti sono superclassi e MyClass è figlia.
# Se non ci sono argomenti MyClass è figlia della classe object.

#Classe vuota
class MyClass():
    pass

print(type(MyClass))

myObj = MyClass()

print(myObj)

#Classe con attributo di classe comune a tutte le istanze
class MyClass2():
    myAttr = 10

m1 = MyClass2()
m2 = MyClass2()

m1.myAttr = 100

print(m1.myAttr)
print(m2.myAttr)

m1.myAttr2=500

print(m1.myAttr2)

#Metodi di istanza
#self = metodo stesso
class MyMethodClass:
    def myMethod(self, message):
        print(id(self), " ", message)
        
m1 = MyMethodClass()
m1.myMethod("ciao uno")

m2 = MyMethodClass()
m2.myMethod("ciao due")

#Attributi di istanza
class MyAttributeInstance:
    def setMessage(self, message):
        self.message = message
    def getMessage(self):
        return self.message
        
m1 = MyAttributeInstance()
m1.setMessage('prova messaggio!')
print(m1.getMessage())
