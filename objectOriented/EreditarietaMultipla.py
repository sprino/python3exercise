############################################################
#######################Exe 1################################
############################################################

#Ereditarietà singola:
print('\n\n----Erreditarieta singola---\n\n')

class MyClass():
    @staticmethod
    def somma(a,b):
        return(a+b)
#Sottoclasse, si passa la classe come argomento.
class MySClass(MyClass):
    #Override del metodo della classe
    @staticmethod
    def somma(a, b):
        c = 10 + MyClass.somma(a, b)
        return c
mysclass = MySClass()
#mysclass è anche un istanza della classe?
print(isinstance(mysclass, MyClass))
print('La somma più 10 é: ' + str(mysclass.somma(25, 25)))

############################################################
#######################Exe 2################################
############################################################
class Classe:
    def setMessage(self,messg):
        self.messg = messg
    def printMessage(self):
        print(self.messg)
#Creo sotto classe
class SottoClasse(Classe):
    pass
sc1 = SottoClasse()
sc1.setMessage("Uso un istanza della sotto classe")
sc1.printMessage()

#Ereditarieta multipla
print('\n\n----Erreditarieta multipla---\n\n')

class BClass:
    b=10;
class CClass:
    c=20;
class AClass(BClass,CClass):
    pass

# A vede gli attributi di BClass e CClass
print("\n\n\n***A vede gli attributi di BClass e CClass***")

class BClass2:
    def bFunc(self):
        print("Sono BClass2...")
class CClass2:
    def cFunc(self):
        print(print("Sono CClass2..."))
class AClass2(BClass2,CClass2):
    pass

aClass2 = AClass2()
aClass2.bFunc()

print('\n')

aClass2.cFunc()

# Funzione in BClass e CClass con lo stesso nome
print('\n\n\n***Funzione in BClass e CClass con lo stesso nome***')

class BClass3:
    def func(self):
        print("Sono BClass3...")
class CClass3:
    def func(self):
        print("Sono CClass3...")
class AClass3(BClass3,CClass3):
    pass

aClass3 = AClass3()
aClass3.func()

print('\n')

aClass3.func()
