# Funzione in BClass e CClass con lo stesso nome
print('\n\n\n***Funzione in BClass e CClass con lo stesso nome***')

class DClass:
    def func(self):
        print("Sono DClass...")
class BClass:
    def func(self):
        print("Sono BClass...")
class CClass:
    def func(self):
        print("Sono CClass...")
class AClass(BClass,CClass):
    pass

aClass = AClass()
aClass.func()

# Funzione solo in CClass
print('\n\n\n***Funzione in BClass e CClass con lo stesso nome***')

class BClass2:
    pass
class CClass2:
    def func(self):
        print("Sono CClass2...")
class AClass2(BClass2,CClass2):
    pass

aClass2 = AClass2()
aClass2.func()

# Funzione solo in CClass e DClass A eredita B e C mentre B eredita D
print('\n\n\n***Funzione solo in CClass e DClass A eredita B e C mentre B eredita D***')
 
class DClass3:
    def func(self):
        print("Sono DClass3...")
class BClass3(DClass3):
    pass
class CClass3:
    def func(self):
        print("Sono CClass3...")
class AClass3(BClass3,CClass3):
    pass
 
aClass3 = AClass3()
aClass3.func()

# Funzione solo in CClass e DClass A eredita B e C, B eredita D e D non ha funzione
print('\n\n\n***Funzione solo in CClass e DClass A eredita B e C, B eredita D e D non ha funzione***')
 
class DClass4:
    pass
class BClass4(DClass4):
    pass
class CClass4:
    def func(self):
        print("Sono CClass4...")
class AClass4(BClass4,CClass4):
    pass
 
aClass4 = AClass4()
aClass4.func()

# Funzione solo in CClass e DClass A eredita B e C, B eredita D e D non ha funzione
print('\n\n\n***Funzione solo in CClass e DClass A eredita B e C, B eredita D e D non ha funzione***')
 
class DClass5:
    pass
class BClass5(DClass5):
    pass
class CClass5:
    pass
class AClass5(BClass5,CClass5):
    pass
 
aClass5 = AClass5()
aClass5.func()
