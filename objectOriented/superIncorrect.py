# __init__ e il costruttore
# se ne faccio l'override nella sottoclasse il costruttore della supercalasse non viene eseguito.
# In questo esempio si ha errore perche' sc1.printMessage non l oesegue perche non trova l'attributo messg.


class Classe:

    def __init__(self, messg):
        self.messg = messg

    def printMessage(self):
        print(self.messg)


# Creo sotto classe
class SottoClasse(Classe):

    def __init__(self, valore):
        self.valore = valore

        
sc1 = SottoClasse(20)
print(sc1.valore)
sc1.printMessage()