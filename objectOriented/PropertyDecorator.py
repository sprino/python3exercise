# Nascondere l'informazione.
# getter e setter
# nascondere gli attributi e tenere pubblici i metodi.


class MyClass():

    def __init__(self, myAtt):
        self.__privAtt = myAtt
        
    @property
    def att(self):
        return self.__privAtt
    
    @att.setter
    def att(self, myAtt):
        self.__privAtt = myAtt


obj = MyClass('Init');
print(obj.att)
obj.att = 'Ciao'
print(obj.att)