# Attribbuti di classe: Comuni a tutte le istanze.


class MyClass():
    counter = 0

    #self = per ogni istanza della classe.
    def __init__(self):
        MyClass.counter += 1
    #Metodo non invocabile dalle istanze.
    #Invocabile solo a livello di classe (cls significa classe).
    @classmethod
    def istanze(cls):
        print(cls.counter)
            
m1 = MyClass()
m2 = MyClass()
m3 = MyClass()
MyClass.istanze()