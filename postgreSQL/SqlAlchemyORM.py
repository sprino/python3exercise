#POSTGRESQL
#Non relazionale no-sql.
#Basato su mongoDB no-sql.
#Libreria ufficiale pyMongo.
#Database orientato ai documenti
#Cluster di server. Noi su un unico server (il nostro pc).
#JSON(Javascript Object Notation) formato di interscambio di dati basato su javascript.
#JSON=Object(Dizionari in python) associazioni tra chiavi e valori + Array (Liste in python) Insieme di dati
# JSON Example
# {
#     "nome":"Mario",
#     "cognome":"Rossi",
#     "eta":"35",
#     "Computer":{        #Nel relazionale avrei due righe oppure una tabella di join!
#         "Comp1":"Asus",
#         "Comp2":"Apple"
#     }
# }
# JSON Example 2
# {
#     "nome":"Mario",
#     "cognome":"Rossi",
#     "eta":"35",
#     "Computer":[ "Asus","Apple"] #Array non oggetto come prima
#     }
# }
#BSON(JSON binario)

# import psycopg2
# 
# conn = psycopg2.connect(
#     # credenziali
#     host="127.0.0.1",
#     dbname="rino_postdb",
#     user="rino_postdb",
#     password="rino_postdb_password",
# )
# curs = conn.cursor()
from sqlalchemy import Column, Index, Integer, Text
import sqlalchemy
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.schema import Sequence


#Connessione al db
connection_string = 'postgresql://rino_postdb:rino_postdb_password@127.0.0.1/rino_postdb'
db = sqlalchemy.create_engine(connection_string)  
engine = db.connect()  
meta = sqlalchemy.MetaData(engine)  

# #test connessione
# print("Test connessione: ")
# result = engine.execute("SELECT ")
# print(result.rowcount)

Base = declarative_base()

class Persone(Base):  
    __tablename__ = 'Persone'
    id = Column(Integer, Sequence('persone_id_seq'), nullable=False, primary_key=True)
    nome = Column(Text)
    cognome = Column(Text)
    computer = Column(JSON)
    __table_args__=(Index('nome',nome.asc()),Index('cognome',cognome.asc()), Index('computer',computer.asc()))
Base.metadata.create_all(engine)
SessionFactory = sessionmaker(engine)
session = SessionFactory()

#Elimino tabella
# Base.metadata.drop_all(engine, meta.tables.get(Persone), checkfirst=True)

#Elimino elementi dalla tabella
deleted = session.query(Persone).delete() 
session.commit()
print("Pulisco tabella, elmino " + str(deleted) + " righe.")
 
persona1 = Persone(
    nome="Mario",
    cognome="Rossi",
    computer={
            "Asus": "Acer",
            "Apple": {"BookPro": 2560, "BookNoPro": 2510},
        })
   
persona2 = Persone(
    nome="Aldo",
    cognome="Baldo",
    computer={
            "Dell":"Sony",
        })

#Aggiungo elementi al tadabase e committo
session.add(persona1)
session.add(persona2)
session.commit()

resultQueryOne = session.query(Persone).filter_by(nome="Mario").first()
print("\nRisultato query mirata:\n" + resultQueryOne.cognome)

resultQueryOne = session.query(Persone).filter_by(nome="Aldo").first()
print("\nRisultato query mirata:\n" + resultQueryOne.computer)
 
resultQueryAll = session.query(Persone).all()
print("\nRisultato query select *:\n " + str(resultQueryAll))

resultUpdate = session.query(Persone).filter_by(nome="Mario").first()
resultUpdate.nome="MarioMario"
session.commit

#Update row
resultQueryUpdate = session.query(Persone).filter_by(cognome="Rossi").first()
resultQueryUpdate.nome="MarioMarioMario"
session.commit()
print("\nRisultato query mirata:\n" + resultQueryUpdate.nome + resultQueryUpdate.computer)
