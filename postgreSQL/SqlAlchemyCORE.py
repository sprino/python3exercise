#POSTGRESQL
#Non relazionale no-sql.
#Basato su mongoDB no-sql.
#Libreria ufficiale pyMongo.
#Database orientato ai documenti
#Cluster di server. Noi su un unico server (il nostro pc).
#JSON(Javascript Object Notation) formato di interscambio di dati basato su javascript.
#JSON=Object(Dizionari in python) associazioni tra chiavi e valori + Array (Liste in python) Insieme di dati
# JSON Example
# {
#     "nome":"Mario",
#     "cognome":"Rossi",
#     "eta":"35",
#     "Computer":{        #Nel relazionale avrei due righe oppure una tabella di join!
#         "Comp1":"Asus",
#         "Comp2":"Apple"
#     }
# }
# JSON Example 2
# {
#     "nome":"Mario",
#     "cognome":"Rossi",
#     "eta":"35",
#     "Computer":[ "Asus","Apple"] #Array non oggetto come prima
#     }
# }
#BSON(JSON binario)

# import psycopg2
# 
# conn = psycopg2.connect(
#     # credenziali
#     host="127.0.0.1",
#     dbname="rino_postdb",
#     user="rino_postdb",
#     password="rino_postdb_password",
# )
# curs = conn.cursor()
from sqlalchemy import Column, Integer, Text  
import sqlalchemy
from sqlalchemy.dialects.postgresql import JSON

connection_string = 'postgresql://rino_postdb:rino_postdb_password@127.0.0.1/rino_postdb'

db = sqlalchemy.create_engine(connection_string)  
engine = db.connect()  
meta = sqlalchemy.MetaData(engine)  

#test connessione
print("\nTest connessione: ")
result = engine.execute("SELECT 1")
print(result.rowcount)

#Creo tabella
j_table = sqlalchemy.Table("jsontable", meta,  
                Column('id', Integer),
                Column('name', Text),
                Column('email', Text),
                Column('doc', JSON))
meta.create_all()

#Pulisco tabella
engine.execute(j_table.delete())

#Inserisco valore e stampo insert
print("\nInsert: ")
statement = j_table.insert().values(
        id=1,
        name="MR. Params",
        email="use@params.com",
        doc={
            "dialect": "params",
            "address": {"street": "Main St.", "zip": 12345},
        },
    )
engine.execute(statement)
print(str(statement))

#Cerco utente
print("\nSelect: ")
find_user = j_table.select().where(j_table.c.name == "MR. Params")
print(engine.execute(find_user).fetchone())

meta.drop_all(engine, meta.tables.get(j_table), checkfirst=False)
