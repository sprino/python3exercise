'''
Created on 17 dic 2019

@author: rino
'''
def get_doppio_gen():
    e = 2
    while (e < 300):
        yield e #La funzione si interrompe e fornisce al chiamante il valore di e
        e *= 2

gen = get_doppio_gen()
print(gen)
print(next(gen)) #equivale a fare gen.__newt__()
print(next(gen))
print(next(gen))
print(next(gen))
print(next(gen))
print(next(gen))
print(next(gen))
print(next(gen))
#print(next(gen))

def get_doppio_gen2():
    e = 2
    while (e < 300):
        yield e #La funzione si interrompe e fornisce al chiamante il valore di e
        e *= 2
        if(e >= 300):
            return

print("\n")

gen2 = get_doppio_gen2()
for el in gen2:
    print(el)