'''
Created on 17 dic 2019

@author: rino
'''
#Liste, pile, code e touple sono contenitori

lista = [1,2,3,4,5,6]

e = 3 in lista

print(str(e))

#Un contenitore è un oggetto iterabile.
#Non tutti gli iterabili sono dei container (vedi un file aperto).

#Iterabile è quando torna un iteratore che permette di eseguire un iterazione

myList = ["primo", "seconto", "terzo"]

it1 = myList.__iter__()

print(type(myList))
print(type(it1))

print(next(it1))
print(next(it1))
print(next(it1))
#print(next(it1))

#Creo un iteratore
print('\nCreo un iteratore\n')

class MyIterator:
    def __iter__(self):
        self.myattr = 2
        return self
    def __next__(self):
        if self.myattr < 300:
            n = self.myattr
            self.myattr *= 2
            return n
        else:
            raise StopIteration
        
myClass = MyIterator()
myiter = iter(myClass)

print(next(myiter))
print(next(myiter))
print(next(myiter))
print(next(myiter))
print(next(myiter))
print(next(myiter))
print(next(myiter))
print(next(myiter))
#print(next(myiter))
print("\n")
for i in myiter:
    print(i)