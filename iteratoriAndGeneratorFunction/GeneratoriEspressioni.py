'''
Created on 17 dic 2019

@author: rino
'''
#Equivalente ad una List comprehension

numbers = [1,2,3,4,5,6,7,8,9]

#List Comprhension è iterabile perche sempre una lista
newList = [n * n for n in numbers if n % 2 == 1]
print(newList)
print(type(newList))

print("\n")

#Generated Expression viene svuotata una volta che viene iterata (Lazy) si usa quando sono molti elementi
newGen = (n * n for n in numbers if n % 2 == 1)
for e in newGen:
    print(e)    
for e in newGen: #E' stato svuotato dalla precedente iterazione
    print(e) 
print(type(newList))