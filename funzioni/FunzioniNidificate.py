#Oggetti eseguibili

#nidificazione semplice
def outer(x,y):
    def sum(a,b):
        return a+b
    print(sum(x, y))   
outer(10, 20)

#nidificazione ritornando l'oggetto funzione interna
def outer2():
    def sum(a,b):
        print(a+b)
    return sum 
f=outer2()
f(100,200)

#funzione come parametro di un altra funziona
def sum(a,b):
    print(a+b)
def myFunc(f,x,y):
    f(x, y)
myFunc(sum, 1000, 2000)