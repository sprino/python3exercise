# Funzioni che generano oggetti funzione anonimi
# lambda arg1, arg2, argN : expression(con gli argomenti)
# Funzioni anonime

print('Normale funzione:')


def myNormalFunc(a, b):
    return(a ** b)


print(myNormalFunc(2, 3))

print('\nFunzione Lambda:')

myLambdaFunc = lambda a, b : a ** b

print(myLambdaFunc(2, 3))

