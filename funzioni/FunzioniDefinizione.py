#Oggetti eseguibili

#definizione:
# def function_name(parameters(opzionale)):
#     statement(suite)

#Parametri posizionali
def myFunc(a,b,c,d):
    print(a,b,c,d)
myFunc(10, 20, 30, 40)

#Parametri non posizionali
def myFunc2(a,b,c,d):
    print(a,b,c,d)
myFunc2(c=10, b=20, a=30, d=40)

#Parametri opzionali
def myFunc3(a,b,c=30,d=40):
    print(a,b,c,d)
myFunc3(a=10, b=20)

#Parametri *args, tupla in uscita
def myFunc4(*args):
    print(args)
myFunc4(10, 20,30,40,50,60,70,80,90,100)

#Parametri posizionali e *args, tupla in uscita
def myFunc5(a,b,*args):
    print(1,2,args)
myFunc5(10, 20,30,40,50,60,70,80,90,100)

#Parametri posizionali e **kwargs, dizionario in uscita
def myFunc6(**kwargs):
    print(kwargs)
myFunc6(a=10,b=20,c=30,d=40,e=50,f=60,g=70,h=80,i=90,j=100)
