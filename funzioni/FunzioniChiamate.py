#Oggetti eseguibili

#definizione:
# def function_name(parameters(opzionale)):
#     statement(suite)


#Passaggio di parametri sempre per riferimento
#oggetto immutabile
def myFunc(x):
    x=10
    print(x)

y=20
myFunc(y)

print(y)


#Passaggio di parametri sempre per riferimento
#oggetto mutabile
def myFunc2(x):
    x['func']=10
    print(x)

d={'a':5}
myFunc2(d)

print(d)

