# Function Decorator
# Funzione che prende un altra funzione, la arricchisce di istruzioni e la restituisce.
# Serve per alterare funzioni già esistenti.


def myFuncDecorator(f):

    def decorator():
        print('Ho decorato')
        f()

    return decorator


def myFunc():
    print('la mia funzione con decoratore manuale!')    


myFunc = myFuncDecorator(myFunc)
myFunc()

print('\n')


@myFuncDecorator
def myFuncConDec():
    print('la mia funzione con decoratore automatico!')


myFuncConDec()
