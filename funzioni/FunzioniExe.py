#Oggetti eseguibili

#INPUT: Due liste una opzionale, l1 ed l2.
#OUTPUT: Nuova lista che contiene gli elementi di l1 non contenuti in l2

def listaDif(l1,l2=[20,30,40]):
    lOut=[]
    for i in l1:
        if i not in l2:
            lOut.append(i)
    return lOut

print(listaDif([1,2,3,10,20]))
