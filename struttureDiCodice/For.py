myList = {1, 2, 3, 4}

for i in myList:
    print(i)
    
myString = '\npython'
for i in myString:
    print(i)

myDict = {'a': 1, 'b':2, 'c': 3}

print('\n')

for i in myDict :
    print(i)

print('\n')

for i in myDict.values() :
    print(i)

print('\n')

for i in myDict.items() :
    print(i)