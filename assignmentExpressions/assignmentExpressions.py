#Introdotte in python 3.8

#PEP 572

#Usano l'operatore tricheco (Walrus Operator) perche somiglia ad un tricheco (:=)

#Uso x nell'if anche se non l'ho assegnata.
print("Semplice somma:\n")
def sum(a,b):
    return a+b

if x := sum(3, 5) > 6:
    print("La somma è maggiore di 6.")
    
print("\n")

print("Semplice while:\n")
myList=[1,2,3,4,5]
while x := len(myList) != 0:
    print(x,myList.pop())

print("\n")

print("Valori di default:\n")
def saluta(nome=(n:="Susanna")):
    print("Ciao",nome)
    print("Ciao",n.upper())
saluta()
    