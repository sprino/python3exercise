#Dizionario con solo chiavi
#Elementi solo univoci
#Operazioni matematiche tra insiemi
#mutabile

mySet = set()
mySet = set([10,20,30,40])

mySet = {10,20,30,40}

mySet.add(50)

print(mySet)

#immutabile

mySetImmutable=frozenset([10,20,30])
#mySetImmutable.add(40)
print(mySetImmutable)

print("\n")

print(30 in mySet)

mySet = {10,20,30,40,50}
mySet2={40,50,60,70}

print(mySet & mySet2)
print(mySet | mySet2)
print(mySet - mySet2)
print("Or esclusivo, tutto tranne quelli comuni:")
print(mySet ^ mySet2)
