#Mutabile
#simile alla mappa in java
#chiave, valore
#type=dict

myDict={}
myDict=dict();

myDict={
    'primo':10,
    'secondo':20,
    'terzo':30
    }

print(myDict)

myDict["quarto"]=40

print(myDict)

del myDict["quarto"]

print(myDict)

#myDict.clear()
#print(myDict)
print("terzo" in myDict)

myDict2 = myDict.copy()

myDict2.clear()

print(myDict2)


#Iteratore
d1 = {10:'a',
      20:'b'}
d2 = {30:'c'}

print("\n")

l1=d1.items()
print(type(l1))
print(l1)

d3 = dict(l1)
print(d3)

print("\n")

l2=d2.items()
print(type(l2))
print(l2)

d4 = dict(l2)
print(d4)

#unione dizionari
d3.update(dict(l2))
print(d3)