#mutabili

myList = []
myList2 = [10, 20, 30]
myList3 = list()

print('\nstampa singolo elemento')

print(myList2[-1])

print('\nlista di liste stampa singolo elemento')

myListList=[[1,2], [3,4], [5,6]]
myListList[1][0] = 500
print(myListList[1][0])
print(myListList)

print('\nslice')

#slice
print(myList2[:2])

print('\ndimensione lista')

#dimensione Lista
print(len(myListList))

print('\ninserimento nella lista')

myList2.insert(2, 500)
print(myList2)

print('\nappend,inserimento in coda')

myListList.append([7,8])
print(myListList)

print('\neliminazione dalla lista')

del myListList[3]
print(myListList)

print('\nè presente nella lista?')

print([500,4] in myListList)

print('\nstesso indirizzo tra oggetti')

myListIndex = [10,20,30]
myListIndex2 = myListIndex
myListIndex[1]=100
print(myListIndex2)

print('\nfunzione copy object')

myListIndex = [10,20,30]
myListIndex2 = myListIndex.copy()
myListIndex[1]=100
print(myListIndex2)

print('\nGeneric Test')

myListIndex = [int('123'),30]
print(123 in myListIndex)
print(myListIndex[myListIndex.index(int('123'))][1])