'''
Created on 27 nov 2019

@author: rino
'''
# Esempio:
# 
# dirA/dirB(__init__)/dirC(__init__)
# 
# I package possono essere annidiati, la prima directory non contiene moduli o script python (dirA) e deve trovarsi
# nel module search path.
# La gerarchia deve terminare con il nome del modulo.
# I file __init__.py sono degli indicatori e possono contenere codice che viene eseguito solo ua volta all'importazione.'
# 
# Importo tutto il modulo:
# 
# import dirB.dirC.modulo1
# 
# Importo solo una funzione presente nel modulo:
# 
# from dirB.dirC.modulo1 import import myFunc