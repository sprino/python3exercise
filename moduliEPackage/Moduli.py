#Sono i file.py
#Nel modulo c'è un namespace non possono esserci due nomi uguali, 
#lo stesso nome si può usare in moduli e quindi in namespace diversi.
#I modili si trovano a livello global.
#I file .py con le classi, funzioni etc si chiamano moduli.
#I file .py con la logica che usa i moduli si chiama script.

#Importazione globale
from moduliEPackage import Operazioni as op

print(type(op))

print(op.somma(10, 5))

#Se il modulo viene importato python assegna all'attributo il nome del modulo, se lo lanci come script singolo lo chiama main.
print(op.__name__)