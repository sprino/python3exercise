'''
Created on 27 nov 2019

@author: rino
'''

def somma(x,y):
    return x+y

#Se il modulo viene importato python assegna all'attributo il nome del modulo, se lo lanci come script singolo lo chiama main.
print(__name__)

if __name__ == '__main__':
    print('Lanciato come script singolo restituisce: ' + str(somma(25, 25)))