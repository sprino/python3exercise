# Eccezzioni come oggetti.
#Eccezzioni custom, creazione e lancio.

def func(a, b):
    return a//b
    
print("\n***Assert se a/b è uguale a 5:\n")

result = func(10,2)
print("Eseguo 10/2 = " + str(result) + "\n")
assert result != 5, "Valori uguali!"