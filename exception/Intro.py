#Eccezzioni come oggetti.
#Possono essere predefinito oppure custom.
#Il programma si ferma nel punto in cui si verifica l'eccezione se questa non è gestita.

def func(a,b):
    return a//b

print("Uso la finzione correttamente 10/2: " + str(func(10, 2)) + "\n")

print("Causo un eccezione 10/0: " + str(func(10, 0)) + "\n")