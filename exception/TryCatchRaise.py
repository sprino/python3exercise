# Eccezzioni come oggetti.
#Eccezzioni custom, creazione e lancio.

def func(a, b):
    return a//b
    
print("\n***Raise per invio a livello superiore:\n")

try:
    print("Eseguo 10/0 : " + str(func(10, 1)) + "\n")
except ZeroDivisionError as e:
    print(e.args)
    raise
else:
    print("Nessun errore, Eseguo else")
finally:
    print("Eseguo finally!")
    
print("\n***Raise lanciata da me:\n")

for i in range(50):
    print(i)
    if i >=10:
        raise IndexError("Superato il numero 10!")