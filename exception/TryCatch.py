# Eccezzioni come oggetti.
# Possono essere predefinito oppure custom.
# Il programma si ferma nel punto in cui si verifica l'eccezione se questa non è gestita.

def func(a, b):
    return a//b

print("Uso la finzione correttamente 10/2: " + str(func(10, 2)) + "\n")

print("\n***Intercetta eccezione generica:\n")

try:
    print("Causo un eccezione 10/0: " + str(func(10, 0)) + "\n")
except:
    print("Errore!")
    
print("\n***Intercetta la divisione per zero:\n")

try:
    print("Causo un eccezione 10/0: " + str(func(10, 0)) + "\n")
except ZeroDivisionError:
    print("Divisione per zero non gestita!")

print("\n***Intercetta due tipi di eccezioni:\n")

try:
    print("Causo un eccezione 10/0: " + str(func(10, 0)) + "\n")
except ZeroDivisionError:
    print("Divisione per zero non gestita!")
except IndentationError:
    print("Indentational error!")
    
print("\n***Intercetta due tipi di eccezioni, modalità compatta:\n")

try:
    print("Causo un eccezione 10/0: " + str(func(10, 0)) + "\n")
except (IndentationError, ZeroDivisionError):
    print("Divisione per zero non gestita!")

print("\n***Eccezzione pralante:\n")

try:
    print("Causo un eccezione 10/0: " + str(func(10, 0)) + "\n")
except ZeroDivisionError as e:
    print(e.args)

print("\n***Finally:\n")

try:
    print("Causo un eccezione 10/0: " + str(func(10, 0)) + "\n")
except ZeroDivisionError as e:
    print(e.args)
finally:
    print("Eseguo finally!")
    
print("\n***Finally & Else:\n")

try:
    print("Eseguo 10/1 : " + str(func(10, 1)) + "\n")
except ZeroDivisionError as e:
    print(e.args)
else:
    print("Nessun errore, Eseguo else")
finally:
    print("Eseguo finally!")