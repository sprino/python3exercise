#Aggiunta alla libreria standard
#Aggiunge in modo automatico ad una classe creata da noi una serie di metodi.
from dataclasses import dataclass

@dataclass
class MyClass:
    nome:str
    cognome:str

#Nome,Cognome difentano field
#Viene generato automaticamente l'init
#aggiunti i metodi __repr__,__eq__(==),__ne__(!=)

mc=MyClass(nome="Mario",cognome="Rossi")
print(mc)

print("\nAggiungo parametri all'annotazione @dataclass")
@dataclass(init=True,repr=False,order=True,frozen=False)
class MyClass2:
    nome:str
    cognome:str

#Nome,Cognome difentano field
#Viene generato automaticamente l'init
#aggiunti i metodi __repr__,__eq__(==),__ne__(!=)

mc2=MyClass2(nome="Mario",cognome="Rossi")
mc3=MyClass2(nome="Mario",cognome="RossiRossi")
print(mc2<mc3) #Perchè order=True
mc3.nome="MarioMario" #Posso modificare perchè frozen è uguale a true
